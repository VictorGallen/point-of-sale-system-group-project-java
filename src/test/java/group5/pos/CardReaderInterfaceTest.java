/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Ragnell
 */
public class CardReaderInterfaceTest {
    
    public CardReaderInterfaceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of reset method, of class CardReaderInterface.
     */
    @Test
    public void testReset() {
        System.out.println("reset");
        CardReaderInterface instance = new CardReaderInterfaceImpl();
        instance.reset();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of abort method, of class CardReaderInterface.
     */
    @Test
    public void testAbort() {
        System.out.println("abort");
        CardReaderInterface instance = new CardReaderInterfaceImpl();
        instance.abort();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of waitForPayment method, of class CardReaderInterface.
     */
    @Test
    public void testWaitForPayment() {
        System.out.println("waitForPayment");
        String payment = "";
        CardReaderInterface instance = new CardReaderInterfaceImpl();
        instance.waitForPayment(payment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readerStatus method, of class CardReaderInterface.
     */
    @Test
    public void testReaderStatus() {
        System.out.println("readerStatus");
        CardReaderInterface instance = new CardReaderInterfaceImpl();
        String expResult = "";
        String result = instance.readerStatus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readerResult method, of class CardReaderInterface.
     */
    @Test
    public void testReaderResult() {
        System.out.println("readerResult");
        CardReaderInterface instance = new CardReaderInterfaceImpl();
        String expResult = "";
        String result = instance.readerResult();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    public class CardReaderInterfaceImpl implements CardReaderInterface {

        public void reset() {
        }

        public void abort() {
        }

        public void waitForPayment(String payment) {
        }

        public String readerStatus() {
            return "";
        }

        public String readerResult() {
            return "";
        }
    }
    
}
