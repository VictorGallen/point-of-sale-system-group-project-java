/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

import java.util.LinkedList;
import javax.swing.JList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
//import group5.pos.Sale;



/**
 *
 * @author Nicolas Ragnell
 */
public class ModelTest {
    
    public PoS_Controller controller;
    public Model model;
    
    public ModelTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
       
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        controller = new PoS_Controller();
        model = new Model(controller);
        model.currentSale = new Sale();
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of sendCode method, of class Model.
     */
    @Test
    public void testSendCode() throws Exception {
        setUp();
        System.out.println("sendCode");
        String barCode = "1234";
        //Model instance = null;
        model.sendCode(barCode);
        
        assertEquals(1,model.currentSale.getProductList().size());
       
    }
  
    /**
     * Test of readPaymentFromString method, of class Model.
     */
    
    @Test
    public void testReadPaymentFromString() {
        System.out.println("readPaymentFromString");
        String cashPayment = "";
        Model instance = null;
        int expResult = 0;
        int result = instance.readPaymentFromString(cashPayment);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of sendCashReceived method, of class Model.
     */
    @Test
    public void testSendCashReceived() {
        System.out.println("sendCashReceived");
        String cashPayment = "";
        JList<String> jListProducts = null;
        Model instance = null;
        instance.sendCashReceived(cashPayment, jListProducts);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of printReceipt method, of class Model.
     */
    @Test
    public void testPrintReceipt() {
        System.out.println("printReceipt");
        Model instance = null;
        instance.printReceipt();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of endSale method, of class Model.
     */
    @Test
    public void testEndSale() {
        System.out.println("endSale");
        Model instance = null;
        instance.endSale();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of generateDummyProduct method, of class Model.
     */
    @Test
    public void testGenerateDummyProduct() {
        System.out.println("generateDummyProduct");
        Model instance = null;
        LinkedList expResult = null;
        LinkedList result = instance.generateDummyProduct();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of update method, of class Model.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        Model instance = null;
        instance.update();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updateSelectedDiscounts method, of class Model.
     */
    @Test
    public void testUpdateSelectedDiscounts() {
        System.out.println("updateSelectedDiscounts");
        JList<String> jListProducts = null;
        int disc = 0;
        Model instance = null;
        instance.updateSelectedDiscounts(jListProducts, disc);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeSelected method, of class Model.
     */
    @Test
    public void testRemoveSelected() {
        System.out.println("removeSelected");
        JList<String> jListProducts = null;
        Model instance = null;
        instance.removeSelected(jListProducts);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFocusedField method, of class Model.
     */
    @Test
    public void testSetFocusedField() {
        System.out.println("setFocusedField");
        int which = 0;
        Model instance = null;
        instance.setFocusedField(which);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFocusedField method, of class Model.
     */
    @Test
    public void testGetFocusedField() {
        System.out.println("getFocusedField");
        Model instance = null;
        int expResult = 0;
        int result = instance.getFocusedField();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clearSelection method, of class Model.
     */
    @Test
    public void testClearSelection() {
        System.out.println("clearSelection");
        JList<String> jListProducts = null;
        Model instance = null;
        instance.clearSelection(jListProducts);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of stashCurrentSale method, of class Model.
     */
    @Test
    public void testStashCurrentSale() {
        System.out.println("stashCurrentSale");
        Model instance = null;
        instance.stashCurrentSale();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of continueCurrentSale method, of class Model.
     */
    @Test
    public void testContinueCurrentSale() {
        System.out.println("continueCurrentSale");
        Model instance = null;
        instance.continueCurrentSale();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of postRequest method, of class Model.
     */
    @Test
    public void testPostRequest() throws Exception {
        System.out.println("postRequest");
        String port = "";
        String device = "";
        String request = "";
        Model instance = null;
        HttpRequests.postRequest(port, device, request);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRequest method, of class Model.
     */
    @Test
    public void testGetRequest() throws Exception {
        System.out.println("getRequest");
        String port = "";
        String device = "";
        String request = "";
        Model instance = null;
        String expResult = "";
        String result = HttpRequests.getRequest(port, device, request);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of roundTotal method, of class Model.
     */
    @Test
    public void testRoundTotal() {
        System.out.println("roundTotal");
        int totalPrice = 0;
        Model instance = null;
        int expResult = 0;
        int result = instance.roundTotal(totalPrice);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of resetSale method, of class Model.
     */
    @Test
    public void testResetSale() {
        System.out.println("resetSale");
        Model instance = null;
        instance.resetSale();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of cardPayment method, of class Model.
     */
    @Test
    public void testCardPayment() {
        System.out.println("cardPayment");
        String payment = "";
        Model instance = null;
        instance.cardPayment(payment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of displayTransaction method, of class Model.
     */
    @Test
    public void testDisplayTransaction() throws Exception {
        System.out.println("displayTransaction");
        String payment = "";
        Model instance = null;
        instance.displayTransaction(payment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of reset method, of class Model.
     */
    @Test
    public void testReset() {
        System.out.println("reset");
        Model instance = null;
        instance.reset();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of abort method, of class Model.
     */
    @Test
    public void testAbort() {
        System.out.println("abort");
        Model instance = null;
        instance.abort();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of waitForPayment method, of class Model.
     */
    @Test
    public void testWaitForPayment() {
        System.out.println("waitForPayment");
        String payment = "";
        Model instance = null;
        instance.waitForPayment(payment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readerStatus method, of class Model.
     */
    @Test
    public void testReaderStatus() {
        System.out.println("readerStatus");
        Model instance = null;
        String expResult = "";
        String result = instance.readerStatus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readerResult method, of class Model.
     */
    @Test
    public void testReaderResult() {
        System.out.println("readerResult");
        Model instance = null;
        String expResult = "";
        String result = instance.readerResult();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of open method, of class Model.
     */
    @Test
    public void testOpen() {
        System.out.println("open");
        Model instance = null;
        instance.open();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of status method, of class Model.
     */
    @Test
    public void testStatus() {
        System.out.println("status");
        Model instance = null;
        String expResult = "";
        String result = instance.status();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
