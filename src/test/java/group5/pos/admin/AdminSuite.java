/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos.admin;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Nicolas Ragnell
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({group5.pos.admin.AdminViewTest.class, group5.pos.admin.AdminControllerTest.class, group5.pos.admin.PoS_group_5_adminTest.class, group5.pos.admin.ModelTest.class})
public class AdminSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
