/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Nicolas Ragnell
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({group5.pos.CashBoxInterfaceTest.class, group5.pos.ProductTest.class, group5.pos.TextToGraphicsTest.class, group5.pos.SaleTest.class, group5.pos.admin.AdminSuite.class, group5.pos.BarCodeInterfaceTest.class, group5.pos.SaleStateTest.class, group5.pos.CardReaderInterfaceTest.class, group5.pos.ModelTest.class, group5.pos.PoS_group_5Test.class, group5.pos.CashierViewTest.class, group5.pos.PoS_ControllerTest.class, group5.pos.CustomerViewTest.class})
public class PosSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
