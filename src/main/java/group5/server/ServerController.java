/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.server;

import group5.server.rest.Application;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Ken_e
 */
public class ServerController implements ProductCatalog,CustomerRegister{
    List<Product> productList;
    public ServerController() {
        initServer();
    }
    
    void initServer(){
        productList=new LinkedList<>();
        ServerUtil.loadFromDb();
        String[] args = {""};
        Application.main(args);
    }

    @Override
    public Product findProductByBarcode(String barcode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Product findProductByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Product> findProductsByKeyword(String keyword) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveProduct(Product product) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer findByBonusCard(String cardNumber, String expiryMonth, String expiryYear) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer findByCustomerNo(String customerNo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveCustomer(Customer customer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
