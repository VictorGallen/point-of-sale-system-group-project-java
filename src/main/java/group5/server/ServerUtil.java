/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.server;

import java.awt.geom.CubicCurve2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Ken_e
 */
public class ServerUtil {
    
    public static List<Product> serverProductList = new LinkedList<>();

    public static String getRequest(String port, String function, String param) { //general get request function for all devices
        try {
            StringBuilder result = new StringBuilder();
            URL url = new URL("http://localhost:" + port + "/" + function + "/" + param);
            System.out.println(url.toString());
            URLConnection conn = url.openConnection();
            HttpURLConnection http = (HttpURLConnection) conn;
            http.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            return result.toString();
        } catch (Exception e) {
            System.err.println("Couldn't connect to: "+port + "/"+function+"/"+param);
            return "";
        }

    }

    static void loadFromDb() {
        //get from catalog
        serverProductList = getProductsFromCatalog();
        serverProductList.add(new Product("sss", 23.00, "HALLELUJAH", null, 2));//TODO remove
        List<Integer> prodListIds  = new LinkedList<>();
        for (Product p : serverProductList){
            System.out.println("DontRemove: "+p.id);
            prodListIds.add(p.id);
        }
        //get if available in sql db
        SQLiteUtil.initDb(prodListIds, null);
        SQLiteUtil.updateProducts();
    }

    private static List<Product> getProductsFromCatalog() {
        String test = getRequest("9003", "rest/findByName", "*");
        System.out.println("test: "+test);
        List<Product> productList = parseProducts(test);
        
        return productList;
    }

    private static List<Product> parseProducts(String test) {
        LinkedList<Product> prodList = new LinkedList<>();
        String[] productSplit = test.split("<product ");
        boolean skipFirst = true;
        for (String s : productSplit){
            if(skipFirst){
                skipFirst=false;
                continue;
            }
            String id = s.split("\"")[1];
            String barCode=s.substring(s.indexOf("<barCode>")+9,s.indexOf("</barCode"));
            String name = s.substring(s.indexOf("<name>")+6, s.indexOf("</name>"));
            Double vat = Double.parseDouble(s.substring(s.indexOf("<vat>")+5,s.indexOf("</vat>")));
            LinkedList<String> keywordsList = new LinkedList<>();
            while(s.contains("<keyword>")){
                String keywordsString = s.substring(s.indexOf("<keyword>")+9,s.indexOf("</keyword>"));
                keywordsList.add(keywordsString);
                s=s.substring(s.indexOf("</keyword>")+10);
            }
            prodList.add(new Product(barCode, vat, name, keywordsList,Integer.parseInt(id)));
        }
        System.out.println("Prodlist size: "+prodList.size());
        for (Product p : prodList){
            System.out.println("ProdId: "+p.id);
            System.out.println(p.keywords.get(0)+" "+p.keywords.size());
        }
        
        return prodList;
    }
}
