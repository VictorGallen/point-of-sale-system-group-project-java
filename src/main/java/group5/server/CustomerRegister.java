/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.server;

/**
 *
 * @author Ken_e
 */
public interface CustomerRegister {
    public Customer findByBonusCard(String cardNumber,String expiryMonth, String expiryYear);
    public Customer findByCustomerNo(String customerNo);
    public void saveCustomer(Customer customer);
}
