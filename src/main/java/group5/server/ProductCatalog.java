/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.server;

import java.util.List;

/**
 *
 * @author Ken_e
 */
public interface ProductCatalog {
    public Product findProductByBarcode(String barcode);
    public Product findProductByName(String name);
    public List<Product> findProductsByKeyword(String keyword);
    public void saveProduct(Product product);
}
