/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.server;

import java.util.List;

/**
 *
 * @author Ken_e
 */
public class Product {
    String barcode;
    double vatPercentage;
    String name;
    int id;
    List<String> keywords;
    int price;

    public Product(String barcode, double vatPercentage, String name, List<String> keywords,int id) {
        this.barcode = barcode;
        this.vatPercentage = vatPercentage;
        this.name = name;
        this.keywords = keywords;
        this.id=id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public double getVatPercentage() {
        return vatPercentage;
    }

    public void setVatPercentage(double vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }
    
    public void addKeyword(String keyword){
        keywords.add(keyword);
    }
    public void removeKeyword(String keyword){
        keywords.remove(keyword);
    }
}
