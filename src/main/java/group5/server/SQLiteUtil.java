/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.server;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kn
 */
public class SQLiteUtil {

    public static final String dbLocation = "jdbc:sqlite:localDependencies/PoS_server.db";

    public static void connect() {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:" + dbLocation;
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public static void createNewDatabase() {


        try (Connection conn = DriverManager.getConnection(dbLocation)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("Database has been initiated.");
                String sql = "CREATE TABLE IF NOT EXISTS products (\n"
                        + "	id integer PRIMARY KEY,\n"
                        + "	price integer NOT NULL "
                        + ");";
                Statement stmt = conn.createStatement();
                // create a new table
                stmt.execute(sql);
                System.out.println("Created table");

                sql = "CREATE TABLE IF NOT EXISTS customers (\n"
                        + "	id integer PRIMARY KEY,\n"
                        + "	points integer NOT NULL "
                        + ");";
                stmt = conn.createStatement();
                stmt.execute(sql);

                DatabaseMetaData md = conn.getMetaData();
                ResultSet rs = md.getTables(null, null, "%", null);
                while (rs.next()) {
                    System.out.println(rs.getString(3));
                }
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createNewTable() {
        // SQLite connection string

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS warehouses (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	capacity real\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(dbLocation);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void initDb(List<Integer> productListIds, List<Integer> customerListIds) {
        createNewDatabase();
        List<Integer> removeIdsProduct=getProductIdsInDb();
        //List<Integer> currentCustomerIds=getCustomerIdsInDb();
        removeIdsProduct.removeAll(productListIds);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SQLiteUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        cleanDb(removeIdsProduct);
    }

    public static void main(String[] args) throws ClassNotFoundException {
        //connect();
        //Class.forName("com.mysql.jdbc.Driver");
        List<Integer> prodListIds = new LinkedList<>();
        prodListIds.add(123);
        prodListIds.add(1234);
        initDb(prodListIds,null);
        
//createNewTable();
        
    }

    private static List<Integer> getProductIdsInDb() {
        String sql = "SELECT id FROM products";
        LinkedList<Integer> productIds= new LinkedList<Integer>();
        try (Connection conn = DriverManager.getConnection(dbLocation);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                productIds.add(rs.getInt("id"));
//                System.out.println(rs.getInt("id") +  "\t" + 
  //                                 rs.getString("name") + "\t" +
    //                               rs.getDouble("capacity"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return productIds;
    }

    //ToDo add customer version
    private static void cleanDb(List<Integer> removeIdsProduct) {
        try (Connection conn = DriverManager.getConnection(dbLocation);
             Statement stmt  = conn.createStatement()){
            for (int i:removeIdsProduct){
                System.out.println("REMOVE: "+i);
                String sql = "Delete from products where id = "+i;
                stmt.execute(sql);
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
    private static void addPriceToDb(int prodId,int price) {
        try (Connection conn = DriverManager.getConnection(dbLocation);
            Statement stmt  = conn.createStatement()){
            String sql = "Insert into products VALUES ("+prodId+","+price+");";
            stmt.execute(sql);
            System.out.println("Added" +prodId);  
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }    

    static void updateProducts() {
        String sql = "SELECT * FROM products";
        LinkedList<Integer> productIds= new LinkedList<Integer>();
        try (Connection conn = DriverManager.getConnection(dbLocation);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                int id = rs.getInt("id");
                int price = rs.getInt("price");
                System.out.println("id/price: "+id+"/"+price);
                for (Product p : ServerUtil.serverProductList){
                    if(p.id==id){
                        p.price=price;
                    }
                }
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
