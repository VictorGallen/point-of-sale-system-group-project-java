/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.server;

/**
 *
 * @author Ken_e
 */
public class BonusCard {
    private String number;
    private String goodThruMonth; 
    private String goodThruYear;
    private boolean blocked;
    private boolean expired;
    private String holderName;

    public BonusCard(String number, String goodThruMonth, String goodThruYear, boolean blocked, boolean expired, String holderName) {
        this.number = number;
        this.goodThruMonth = goodThruMonth;
        this.goodThruYear = goodThruYear;
        this.blocked = blocked;
        this.expired = expired;
        this.holderName = holderName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getGoodThruMonth() {
        return goodThruMonth;
    }

    public void setGoodThruMonth(String goodThruMonth) {
        this.goodThruMonth = goodThruMonth;
    }

    public String getGoodThruYear() {
        return goodThruYear;
    }

    public void setGoodThruYear(String goodThruYear) {
        this.goodThruYear = goodThruYear;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }
}
