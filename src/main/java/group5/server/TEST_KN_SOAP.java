/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author Ken_e
 */
public class TEST_KN_SOAP {
    public static void main(String[] args) throws IOException {
        String test = getRequest("9003", "rest/findByName", "*");
        System.out.println(test);
    }
    
    public static String getRequest(String port, String function, String param) throws MalformedURLException, IOException { //general get request function for all devices
        StringBuilder result = new StringBuilder();
        URL url = new URL("http://localhost:" + port + "/" + function + "/" + param);
        System.out.println(url.toString());
        URLConnection conn = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) conn;
        http.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }
}
