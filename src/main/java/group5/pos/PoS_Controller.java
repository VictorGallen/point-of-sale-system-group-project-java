/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

import java.io.IOException;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class PoS_Controller {

    private CashierView cashier;
    private CustomerView customer;
    private Model model;
    DecimalFormat df = new DecimalFormat("0.00##");

    public PoS_Controller() {

        try { //set nimbus UI
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        cashier = new CashierView(this);
        customer = new CustomerView(this);
        model = new Model(this);
        model.update();
        cashier.getResultField().setTransferHandler(null);
    }

    public CashierView getCashier() {
        return cashier;
    }

    public boolean calculateSubString() {
        String field = cashier.getTextFieldCash();
        int dot = field.indexOf(".");
        String afterdec = field.substring(dot + 1);
        if (dot != -1 && afterdec.length() > 1) {
            return false;
        } else {
            return true;
        }
    }

    public void setFocusedField(int cash) {
        model.setFocusedField(cash);
    }

    public int getFocusedField() {
        // if true, cash textfield is in focus
        // else barcode is in focus
        int focusedComp = model.getFocusedField();
        return focusedComp;
    }

    void discountSelected(JList<String> jListProducts, int discInt) {
        model.updateSelectedDiscounts(jListProducts, discInt);
    }

    void removeSelected(JList<String> jListProducts) {
        model.removeSelected(jListProducts);
    }

    void clearSelection(JList<String> jListProducts) {
        model.clearSelection(jListProducts);
    }

    void sendBarCode(String textFieldCode) throws IOException, ParserConfigurationException, SAXException {
        model.sendCode(textFieldCode);
    }

    public CustomerView getCustomer() {
        return customer;
    }

    void stashCurrentSale() {
        model.stashCurrentSale();
    }

    void continueCurrentSale() {
        model.continueCurrentSale();
    }

    public Model getModel() {
        return model;
    }

    void cashPayment(String textFieldCash, JList<String> jListProducts) throws IOException { //round to nearest 5 cents, open cash box
        model.sendCashReceived(textFieldCash, jListProducts); //update sale price
    }

    void cardPayment(String textFieldPayment, JList<String> jListProducts) throws IOException {
        model.cardPayment(textFieldPayment);
    }

    void endSale() {
        model.endSale();
    }

    void receipt() {
        customer.getReceiptButton().setEnabled(false);//TODO possibly should check if succefull print
        model.printReceipt();
    }

    //Called after every update in the Model
    void updateUi() {
        Sale currentSale = model.currentSale;
        SaleState currentState = currentSale.state;
        updateCustomerUi(currentSale, currentState);
        updateCashierUi(currentSale, currentState);
    }

    //UI for customer
    void updateCustomerUi(Sale currentSale, SaleState currentState) {
        JButton receiptB = customer.getReceiptButton();

        //Get information about the LastProduct so it can be shown in the customerview
        if (!currentSale.getProductList().isEmpty()) {
            Product LastProduct = currentSale.getProductList().getLast();
            customer.getLastProd().setText(LastProduct.toString());
        }
        //Show information to customer (showing exact leftToPay if differs from what roundedLeftToPay is)

        int change = currentSale.calculateChange();
        change = model.roundTotal(change);
        String changeString = "";
        if (change > 0) {
            changeString = " Change: " + df.format((change) / 100.0) + "€";
        }
        int exactLeftToPay = currentSale.calculateLeftToPay();
        int roundedLeftToPay = model.roundTotal(exactLeftToPay);
        String exactLeftToPayString = "";
        if (!(exactLeftToPay == roundedLeftToPay)) {
            exactLeftToPayString = "(" + df.format(exactLeftToPay / 100.0) + "€)";
        }
        String roundedLeftToPayString = "Total: " + df.format(roundedLeftToPay / 100.0) + "€ ";

        customer.getTextFieldMoney().setText(roundedLeftToPayString + exactLeftToPayString + changeString);
        if (currentState.equals(SaleState.ongoing)) {
            receiptB.setEnabled(false);
        } else {
            customer.getReceiptButton().setEnabled(true);
            if (currentSale.alreadyPaid == 0) {//checking if new or done sale
                receiptB.setEnabled(false);
            } else {
                receiptB.setEnabled(true);
            }
        }
    }

    //UI for cashier
    void updateCashierUi(Sale currentSale, SaleState currentState) {
        JButton cashB = cashier.getjButtonCash();
        JButton cardB = cashier.getjButtonCard();
        JButton endB = cashier.getEndSaleButton();
        JTextField result = cashier.getResultField();

        //updateButtons
        if (currentState.equals(SaleState.ongoing)) {
            cashB.setEnabled(true); //TODO possibly, can't disable bacause what if you put in 50 and they pay with 500, you have to be able to add
            cardB.setEnabled(true);
            endB.setEnabled(false);
        } else {
            result.setText("");
            cashB.setEnabled(false);
            cardB.setEnabled(false);
            if (currentSale.productList.isEmpty()) {
                endB.setEnabled(false);
            } else {
                endB.setEnabled(true);
            }
        }

        //updateList
        cashier.updateList(currentSale.getProductList());

        //updateTotal
        cashier.setTotalField(currentSale.displayPrice());

        //updateChange
        if (currentState.equals(SaleState.ongoing)) {
            cashier.getTextFieldChange().setText("");
        } else {
            if (currentSale.alreadyPaid == 0) {
                cashier.getTextFieldChange().setText("");
            } else {

                int change = currentSale.calculateChange();
                change = model.roundTotal(change);
                cashier.getTextFieldChange().setText(df.format((change) / 100.0));
            }
        }

        //updateToPay
        if (currentState.equals(SaleState.ongoing)) {
            int leftToPay = currentSale.calculateLeftToPay();
            if (currentSale.paidWithCash==0) {
                leftToPay = model.roundTotal(leftToPay);
            }
            cashier.getToPayField().setText(df.format((leftToPay) / 100.0));
        } else {
            cashier.getToPayField().setText("0.00");
        }

        //update Resultfields
        if (currentSale.paidWithCash==1 || currentSale.paidWithCash==3) {
            cashier.getResultField().setText(currentSale.cashResult);
        } else {
            cashier.getResultField().setText(currentSale.cardResult);
        }
        cashier.getBonusField().setText(currentSale.bonusResult);
    }

    void resetSale() {
        model.resetSale();
    }

    void abortTransaction() {
        model.abort();
    }

    void clearTransaction() {
        model.reset();
    }

}
