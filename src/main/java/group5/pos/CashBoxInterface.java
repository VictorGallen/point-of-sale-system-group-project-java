/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

/**
 *
 * @author Nicolas Ragnell
 */
public interface CashBoxInterface {

    public void open();

    public String status();
}
