/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos.admin;

import javax.swing.JList;

/**
 *
 * @author kn
 */
public class AdminController {
    private AdminModel model;
    private AdminView adminView;
    
    public AdminController() {
        model = new AdminModel(this);
        adminView = new AdminView(this);
    }

    void updateUi() {
        //refreshList mm.
    }

    void definePrice(JList<String> productList) {
        model.definePrice(productList);
    }

    void clearSelection(JList<String> productList) {
        model.clearSelection(productList);
    }

    void specialOffers(JList<String> productList) {
        model.specialOffers(productList);
    }
    
}
