/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos.admin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.swing.JList;

/**
 *
 * @author kn
 */
public class AdminModel {
    AdminController adminController;
    //List<Product> productList;//Make a new Product in admin
    //
    public AdminModel(AdminController controller) {
        this.adminController = controller;
    }
    
    void update(){
        //Do all the update 
        adminController.updateUi();
    }

    void clearSelection(JList<String> productList) {
        productList.clearSelection();
    }

    void specialOffers(JList<String> productList) {
        System.out.println(productList.getSelectedValues());
    }

    void definePrice(JList<String> productList) {

    }
    public void postRequest(String port, String device, String request) throws MalformedURLException, IOException {
        URL url = new URL("http://localhost:" + port + "/" + device + "/" + request);
        URLConnection conn = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) conn;
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.getResponseCode();
    }

    public String getRequest(String port, String device, String request) throws MalformedURLException, IOException { //general get request function for all devices
        StringBuilder result = new StringBuilder();
        URL url = new URL("http://localhost:" + port + "/" + device + "/" + request);
        URLConnection conn = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) conn;
        http.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }

}
