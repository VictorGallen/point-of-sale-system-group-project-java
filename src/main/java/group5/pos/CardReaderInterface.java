/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

/**
 *
 * @author DS
 */
public interface CardReaderInterface {
    public void reset();
    public void abort();
    public void waitForPayment(String payment);
    
    public String readerStatus();
    public String readerResult();
}
