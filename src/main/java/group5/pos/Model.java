/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Victor Gallen
 */
public class Model implements CashBoxInterface, CardReaderInterface {

    private PoS_Controller controller;
    boolean aborted = false;
    public Sale currentSale;
    LinkedList<Sale> saleQueue;
    boolean cashInFocus;
    boolean barcodeInFocus;
    boolean discountInFocus;
    final int MAX_SALES_SIMULTANEOUSLY = 3;
    final int MAX_CASH_PAYMENT = 500;

    public Model(PoS_Controller controller) {
        this.controller = controller;

        //making a sale and a saleQueue, initiating with dummy products
        currentSale = new Sale();
        saleQueue = new LinkedList<Sale>();
        saleQueue.add(currentSale);
        currentSale.setProductList(generateDummyProduct());
    }

    public void sendCode(String barCode) throws IOException, ParserConfigurationException, SAXException {
        //send to API a request of the item
        try{
            String xmlResponse = HttpRequests.getRequest("9003", "rest/findByBarCode", barCode);
            Document document = getDOM(xmlResponse);
        
            String productName = document.getDocumentElement().getElementsByTagName("name").item(0).getTextContent();
            currentSale.addToProductList(new Product(productName, 235, barCode));
            update();
        }
        catch(FileNotFoundException ex){
            JOptionPane.showMessageDialog(null, "The barcode doesn't match any products in the product catalog.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    int readPaymentFromString(String cashPayment) {
        if (cashPayment.equals("")) {
            return 0;
        } else if (cashPayment.equals(".")) {
            return 0;
        }
        boolean isInEuro = !cashPayment.contains(".");
        int cashReceived = Integer.parseInt(cashPayment.replaceAll("[.]", ""));

        if (isInEuro) {
            cashReceived *= 100;
        }
        return cashReceived;
    }

    void sendCashReceived(String cashPayment, JList<String> jListProducts) {
        controller.getCashier().getResultField().setText("");
        currentSale.cashResult = "";
        boolean success = true;
        if (cashPayment.equals("")) {
            currentSale.cashResult = "Empty Payment field";
            success = false;
        } else if (cashPayment.equals(".")) {
            currentSale.cashResult = "Empty Payment field";
            success = false;
        } else {
            try {
                Double.parseDouble(cashPayment);
            } catch (NumberFormatException e) {
                currentSale.cashResult = "Error in Payment field";
                success = false;
            }
        }
        if (!success) {
            currentSale.paidWithCash = 3;
            update();
            return;
        }
        //to be expanded upon if we want it fancy
        int cashReceived = readPaymentFromString(cashPayment);
        if (!(cashReceived % 5 == 0)) {
            currentSale.cashResult = ("Only even 5 cents payments");
            success = false;
        } else if (cashReceived > MAX_CASH_PAYMENT * 100) {
            currentSale.cashResult = ("Max 500€ per payment");
            success = false;
        } else if (cashReceived == 0) {
            currentSale.cashResult = ("Empty Payment field");
            success = false;
        }
        if (success) {
            currentSale.alreadyPaid += cashReceived;
            open();
            currentSale.paidWithCash = 1;
        } else {
            currentSale.paidWithCash = 3;
        }

        update();
    }

    public void printReceipt() { //TODO: receipt API
        System.out.println("print receipt for customer");
        StringBuilder printText = new StringBuilder();
        printText.append("Food shop\n----------\n");
        for(Product p : currentSale.getProductList()){
            printText.append(p.getProdName() + "    "+ (p.getProdPrice()/100.0)+"€\n");
        }
        printText.append("\n---------\nTotal: "+currentSale.calculatePriceOfList()/100.0+"€ \nChange: "+currentSale.calculateChange()/100.0 +"€");
        printText.append("\n---------\nHave a nice day!");
        TextToGraphics.createImageWithText(printText.toString());
    }

    public void endSale() {
        for (int i = 0; i < currentSale.productList.size(); i++) { //remove every element from product list
            currentSale.removeFromList(i);
            i--;
        }

        controller.getCashier().updateList(currentSale.getProductList());
        int currentSaleIndex = saleQueue.indexOf(currentSale);
        saleQueue.remove(currentSale);

        reset(); //resets card reader
        currentSale = null; //destroy sale object

        currentSale = new Sale();
        saleQueue.add(currentSaleIndex, currentSale);
        update();
    }

    //make dummy products
    public LinkedList generateDummyProduct() {
        LinkedList<Product> dummyList = new LinkedList<Product>();
        dummyList.add(new Product("Apple", 135, "0123"));
        dummyList.add(new Product("Orange", 30, "1234"));
        dummyList.add(new Product("Boi", 100, "1244656"));
        return dummyList;
    }

    void update() {
        updateSale();
        controller.updateUi();
    }

    //Updates selected products with discounts from product list
    void updateSelectedDiscounts(JList<String> jListProducts, int disc) {
        if(disc < 101 && disc > 0){
        int[] selectedIndex = jListProducts.getSelectedIndices();
        for (int i = selectedIndex.length - 1; i > -1; i--) {
            currentSale.discount(selectedIndex[i], disc);
        }
        }
        update();
    }

    //Remove selected products from active product list
    void removeSelected(JList<String> jListProducts) {
        int[] selectedIndex = jListProducts.getSelectedIndices();
        for (int i = selectedIndex.length - 1; i > -1; i--) {
            currentSale.removeFromList(selectedIndex[i]);
        }
        update();
    }

    public void setFocusedField(int which) {
        if (which == 1) {
            cashInFocus = true;
            barcodeInFocus = false;
            discountInFocus = false;
        } else if (which == 2) {
            barcodeInFocus = true;
            cashInFocus = false;
            discountInFocus = false;
        }
        else{
            discountInFocus = true;
            cashInFocus = false;
            barcodeInFocus = false;
        }
    }

    public int getFocusedField() {
        if (cashInFocus) {
            return 1;
        } else if(barcodeInFocus) {
            return 2;
        }
        else{
            return 3;
        }
    }

    void clearSelection(JList<String> jListProducts) {
        jListProducts.clearSelection();
    }

    //stash/continue Current sale
    //as implemented, when a sale is done you should just add the new sale as the first in the que
    void stashCurrentSale() {
        JButton stashB = controller.getCashier().getStashSaleButton();
        JButton continueB = controller.getCashier().getContinueSaleButton();
        int currentSaleIndex = saleQueue.indexOf(currentSale);
        int sizeOfSaleQueue = saleQueue.size();
        if (currentSaleIndex >= MAX_SALES_SIMULTANEOUSLY - 2) {
            stashB.setEnabled(false);
        }
        if (currentSaleIndex == sizeOfSaleQueue - 1) {
            currentSale = new Sale();
            saleQueue.addLast(currentSale);
        } else {
            currentSale = saleQueue.get(currentSaleIndex + 1);
        }

        continueB.setEnabled(true);
        update();
    }

    void continueCurrentSale() {//TODO use this
        JButton stashB = controller.getCashier().getStashSaleButton();
        JButton contuniueB = controller.getCashier().getContinueSaleButton();
        int currentSaleIndex = saleQueue.indexOf(currentSale);
        if (currentSaleIndex == 1) {
            contuniueB.setEnabled(false);
        }
        currentSale = saleQueue.get(currentSaleIndex - 1);
        stashB.setEnabled(true);
        update();
    }

    int roundTotal(int totalPrice) { //method which round the total sum to nearest five cents (if we pay in cash)
        int remainder = totalPrice % 5;
        if (remainder < 3) {
            return totalPrice - remainder;
        } else {
            return totalPrice + 5 - remainder;
        }
    }

    private void updateSale() {
        if (currentSale.calculateLeftToPay() < 3 || currentSale.calculatePriceOfList() == 0) {
            currentSale.state = SaleState.endable;
        } else {
            currentSale.state = SaleState.ongoing;
        }
    }

    void resetSale() {
        int currentSaleIndex = saleQueue.indexOf(currentSale);
        saleQueue.remove(currentSaleIndex);

        currentSale = new Sale();
        saleQueue.add(currentSaleIndex, currentSale);
        update();
    }

    void cardPayment(String payment) { //connect to CardReader API
        boolean success = true;
        boolean payEverything = false;
        if (payment.equals("")) {
            payEverything = true;
        } else if (payment.equals(".")) {
            success = false;
            currentSale.cashResult = "EMPTY PAYMENT FIELD";
        } else {
            try {
                Double.parseDouble(payment);
            } catch (NumberFormatException e) {
                currentSale.cashResult = "PAYMENT FIELD ERROR";
                success = false;
            }
        }
        if (!success) {
            currentSale.paidWithCash = 2;
            update();
            return;
        }
        if (!payEverything) {
            if (Double.parseDouble(payment) > currentSale.calculateLeftToPay() / 100.00) {
                currentSale.paidWithCash = 2;
                currentSale.cardResult = "CARD PAYMENT MAX " + currentSale.calculateLeftToPay() / 100.00;
                update();
            }
            aborted = false;
            waitForPayment(payment);
        }else{
            payment = String.valueOf(currentSale.calculateLeftToPay());
            aborted = false;
            waitForPayment(String.valueOf(Double.parseDouble(payment) / 100.00));
        }
        String realPayment = payment;
        currentSale.paidWithCash = 0;
        //start thread
        ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
        exec.scheduleAtFixedRate(() -> {
            if (readerResult().length() > 0) { //when the customer has paid with card successfully or cashier has aborted payment
                exec.shutdownNow(); //shut down thread
                try { //we put the transaction result to the cashier view
                    displayTransaction(realPayment);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (aborted) { //when cashier clicks abort transaction, we shut down the thread
                exec.shutdownNow();
            }
        }, 2, 2, TimeUnit.SECONDS);
    }

    public void displayTransaction(String payment) throws ParserConfigurationException, SAXException, IOException {
        Document document = getDOM(readerResult());
        

        try { //if we find a payment state tag, the customer swiped a credit card
            String transactionResult = document.getDocumentElement().getElementsByTagName("paymentState").item(0).getTextContent();
            currentSale.cardResult = transactionResult;
            
            try { //if the customer swiped a combined card
                String bonusResult = document.getDocumentElement().getElementsByTagName("bonusState").item(0).getTextContent();
                String bonusNumber = document.getDocumentElement().getElementsByTagName("bonusCardNumber").item(0).getTextContent();
                String expiryMonth = document.getDocumentElement().getElementsByTagName("goodThruMonth").item(0).getTextContent();
                String expiryYear = document.getDocumentElement().getElementsByTagName("goodThruYear").item(0).getTextContent();
            
                try{ //we check if the bonus card number is valid
                    String xmlResponse2 = HttpRequests.getRequest("9004", "rest/findByBonusCard", bonusNumber + "/" + expiryYear + "/" + expiryMonth);
                    Document document2 = getDOM(xmlResponse2);
                    currentSale.bonusResult = bonusResult;
                }
                catch (FileNotFoundException exc){ //in case we cannot find the bonus number in the customer register, the state should be unsupported
                    currentSale.bonusResult = "UNSUPPORTED_CARD";
                    update();
                    reset();
                }
                
            } catch (NullPointerException ex) { //customer didn't swipe combined card, move on
            }

            if (transactionResult.equals("ACCEPTED")) { //if the transaction was succesful
                int intPayment = Integer.parseInt(payment.replaceAll("[.]", "")); //we multiply by 100 because our object representation of price is in integers
                currentSale.alreadyPaid += intPayment;
            }
            update();
            
        } catch (NullPointerException ex) { //otherwise (s)he swiped only the bonus card

            String bonusResult = document.getDocumentElement().getElementsByTagName("bonusState").item(0).getTextContent();
            String bonusNumber = document.getDocumentElement().getElementsByTagName("bonusCardNumber").item(0).getTextContent();
            String expiryMonth = document.getDocumentElement().getElementsByTagName("goodThruMonth").item(0).getTextContent();
            String expiryYear = document.getDocumentElement().getElementsByTagName("goodThruYear").item(0).getTextContent();
            
            try{
                String xmlResponse3 = HttpRequests.getRequest("9004", "rest/findByBonusCard", bonusNumber + "/" + expiryYear + "/" + expiryMonth);       
                Document document3 = getDOM(xmlResponse3);           
            }
            catch (FileNotFoundException exc){ //in case we cannot find the bonus number in the customer register, the state should be unsupported
                currentSale.bonusResult = "UNSUPPORTED_CARD";
                update();
                reset();
                return;
            }
            currentSale.bonusResult = bonusResult;
            update();
            reset(); //reset card reader after bonus card swipe
        }
    }
    public Document getDOM(String xmlResponse) throws ParserConfigurationException, SAXException, IOException{
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xmlResponse));
        Document document = builder.parse(is);
        return document;
    }

    @Override
    public void reset() {
        try {
            HttpRequests.postRequest("9002", "cardreader", "reset");
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void abort() {
        try {
            aborted = true;
            HttpRequests.postRequest("9002", "cardreader", "abort");
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void waitForPayment(String payment) {
        try {
            String paymentParameter = "amount=" + payment;
            byte[] postdata = paymentParameter.getBytes(StandardCharsets.UTF_8);
            URL url = new URL("http://localhost:9002/cardreader/waitForPayment");
            URLConnection conn = url.openConnection();
            HttpURLConnection http = (HttpURLConnection) conn;
            http.setDoOutput(true);
            http.setInstanceFollowRedirects(false);

            http.setRequestMethod("POST");
            http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            http.setRequestProperty("charset", "utf-8");
            http.setRequestProperty("Content-Length", Integer.toString(postdata.length));
            http.setUseCaches(false);

            try (DataOutputStream wr = new DataOutputStream(http.getOutputStream())) {
                wr.write(postdata);
                http.getResponseCode();
            } catch (ConnectException ex) { //in case the card reader is not connected
                JOptionPane.showMessageDialog(null, "The card reader is not connected.", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String readerStatus() {
        try {
            return HttpRequests.getRequest("9002", "cardreader", "status");
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    @Override
    public String readerResult() {
        try {
            return HttpRequests.getRequest("9002", "cardreader", "result");
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    @Override
    public void open() {
        try {
            HttpRequests.postRequest("9001", "cashbox", "open");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "The cash box is not connected to the system.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public String status() {
        try {
            return HttpRequests.getRequest("9001", "cashbox", "status");
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }


}
