/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

/**
 *
 * @author kn
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;

class TextToGraphics extends JPanel {

    public static void createImageWithText(String text) {
        String[] rows = text.split("\n");
        int height = rows.length * 20 + 40;
        BufferedImage bufferedImage = new BufferedImage(200, height, BufferedImage.TYPE_INT_ARGB);
        Graphics g = bufferedImage.getGraphics();
        g.setColor(Color.BLACK);

        int tmpHeight = 0;
        for (String s : rows) {
            g.drawString(s, 20, tmpHeight += 20);
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        Date date = new Date();
        File outputfile = new File("receipt_" + dateFormat.format(date) + ".png");
        try {
            ImageIO.write(bufferedImage, "png", outputfile);
        } catch (IOException ex) {
            Logger.getLogger(TextToGraphics.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
