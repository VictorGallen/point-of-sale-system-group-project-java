/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

import java.util.List;

/**
 *
 * @author RR
 */
public class Product {

    String name;
    int originalPrice;
    int price; //price is divided so that 3.45 € is written as 345.
    String barcode;
    int discount;
    
    double vatPercentage;
    String id;
    List<String> keywords;

    public Product(String Prodname, int Prodprice, String Prodbarcode) {
        this.name = Prodname;
        this.price = Prodprice;
        this.originalPrice=this.price;
        this.barcode = Prodbarcode;
        this.discount = 100;
    }

    public int getProdPrice() {
        return this.price;
    }

    public String getProdName() {
        return this.name;
    }

    public String getProdBarcode() {
        return this.barcode;
    }

    public String toString() {
        return this.name + ", " + displayPrice(this.price) + " €";
    }
    
    public int getDiscount(){
        return this.discount;
    }
    
    public void setDiscountedPrice(int disc){
        this.discount = 100 - disc;
        if(this.discount == 0){
            this.price = 0;
        }
        else{
        int tempPrice = this.originalPrice * this.discount;
        this.price = tempPrice/100;
        }
    }
    
    public String displayPrice(int price) { //method which divides product price to a euro and a cent part
        if(price == 0){
            return "0";
        }
        else{
        String visiblePrice = price / 100.0 + "";
        return visiblePrice;
        }
    }
}
