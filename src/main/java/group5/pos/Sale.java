/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

import java.util.LinkedList;

/**
 *
 * @author Ken_e
 */
public class Sale {

    public LinkedList<Product> productList;
    public int alreadyPaid;
    public SaleState state;
    public int paidWithCash; //0=false 1=true 2=false with errors 3=true with errors
    //add plussa kort = true/false, mm
    public String cardResult;
    public String bonusResult;
    String cashResult;

    public Sale() {
        state = SaleState.ongoing;
        productList = new LinkedList<Product>();
        alreadyPaid = 0;
        paidWithCash = 1;
        cardResult = "";
        bonusResult = "";
        cashResult = "";
    }

    public LinkedList<Product> getProductList() {
        return productList;
    }

    public void setProductList(LinkedList<Product> productList) {
        this.productList = productList;
    }

    public void removeFromList(int index) {
        productList.remove(index);
    }

    public void addToProductList(Product prod) {
        productList.add(prod);
    }

    public void discount(int i, int disc) {
        productList.get(i).setDiscountedPrice(disc);
    }

    String displayPrice() { //method which divides total price to a euro and a cent part and converts it to string

        String strPrice = String.valueOf(calculatePriceOfList());
        if (strPrice.equals("0")) { //we cannot split strings of length 0.
            return "0.00";
        }

        String euroPart = strPrice.substring(0, strPrice.length() - 2); //the last two digits are cents
        String centPart = strPrice.substring(strPrice.length() - 2, strPrice.length());

        String visiblePrice = euroPart + "." + centPart;

        return visiblePrice + "€";
    }

    public int calculatePriceOfList() {
        int price = 0;
        for (Product p : productList) {
            price += p.getProdPrice();
        }
        return price;
    }

    public int calculateChange() {
        if (calculateLeftToPay() > 0) {
            return 0;
        }

        int change = alreadyPaid - calculatePriceOfList();

        return change;
    }

    String getChange() {
        return "" + (calculateChange() / 100.0);
    }

    public int calculateLeftToPay() {
        int leftToPay = calculatePriceOfList() - alreadyPaid;
        if (leftToPay < 0) {
            return 0;
        }
        return leftToPay;
    }
}
