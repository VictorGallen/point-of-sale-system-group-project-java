/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author DS
 */
public class HttpRequests {
    public static void postRequest(String port, String device, String request) throws MalformedURLException, IOException {
        URL url = new URL("http://localhost:" + port + "/" + device + "/" + request);
        URLConnection conn = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) conn;
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.getResponseCode();
    }

    public static String getRequest(String port, String device, String request) throws MalformedURLException, IOException { //general get request function for all devices
        StringBuilder result = new StringBuilder();
        URL url = new URL("http://localhost:" + port + "/" + device + "/" + request);
        URLConnection conn = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) conn;
        http.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }
}
