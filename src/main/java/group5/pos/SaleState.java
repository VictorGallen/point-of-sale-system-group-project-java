/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5.pos;

/**
 *
 * @author kn
 */
public enum SaleState {
    ongoing("ONGOING"),
    endable("ENDABLE");

    private final String name;

    private SaleState(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false 
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
