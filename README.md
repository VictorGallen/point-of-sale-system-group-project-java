# What is this repository

A group project for a course in software development in practice.
As a group we are creating a PoS (Point of Sale) system that is basically a fully working cash register.
We are using Netbeans integrated GUI builder for the visual part of this project.
The course will be completed by 1.3.2018, full instructions on how to get the project working on your local machine will be released by then.
This will included all dependencies, libraries, requirements and external files necessary for the software to work properly.

# Which commits are mine?

The commits with the username "vigallen" or "ViciGallen" are my commits. I changed account permanently to ViciGallen midproject.
From now on all my commits will be using the name "ViciGallen".

# Who to contact?

If you have more questions regarding this repository contact me @ gallen.victor@gmail.com
Happy coding!
